package com.example.sergionsignal.service

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.util.Log
import com.onesignal.OSNotification
import com.onesignal.OSNotificationReceivedEvent
import com.onesignal.OneSignal

class NotificationServiceExtension : Service(),
    OneSignal.OSNotificationWillShowInForegroundHandler {

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        // Retrieve data from SharedPreferences


        // Handle the notification event
        OneSignal.setNotificationWillShowInForegroundHandler(this)

        return START_STICKY
    }
    override fun notificationWillShowInForeground(notificationReceivedEvent: OSNotificationReceivedEvent?) {
        // Check if notification event is not null
        Log.e("aaa","aaa")

        notificationReceivedEvent?.let {

            // Access notification data
            val notification: OSNotification = it.notification
            Log.e("aaa","${notification.title}")
            // val sharedPreferences = getSharedPreferences("YourPreferences", Context.MODE_PRIVATE)

            // Perform any processing or filtering based on notification data
            var titre = notification.title.split(",")[1]
            if (titre == "montritre") {
                Log.e("aaa","rtt")
                null
                // Prevent the notification from being displayed
                // it.complete(notification)
                println("Notification filtered: ${notification.title}")
            }
        }
    }


}