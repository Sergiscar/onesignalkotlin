package com.example.sergionsignal

import android.app.Application
import androidx.appcompat.app.AppCompatDelegate
import com.example.sergionsignal.service.NotificationServiceExtension
import com.onesignal.OneSignal


class App  : Application(){

  //  @Inject
  //  lateinit var preferences: PreferenceHelper

 //   @Inject lateinit var workerFactory: HiltWorkerFactory

    companion object {
        var isAppRunning = false
    }

    override fun onCreate() {
        super.onCreate()
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        OneSignal.initWithContext(this)
        OneSignal.setAppId("e4cd3195-0f78-45c8-b416-5a2af40d13d1")
        OneSignal.setNotificationWillShowInForegroundHandler(NotificationServiceExtension())


    }


}